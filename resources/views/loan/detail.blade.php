@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <a href="{{route('loan_dashboard')}}">&lg; Dashboard</a>
        </div>
        <div class="col-md-8">
            <p>User: {{$loan->user->email}}</p>
            <p>Amount: {{$loan->amount}}</p>
            <p>Term: {{$loan->term_by_week}} weeks</p>
            <p>Status: {{$loan->status}}</p>
        </div>
        <div>
            @if ($loan->isRepayable)
            <form method="POST" action="{{ route('loan_repay', ['code' => $loan->code]) }}">
                @csrf
                <input type="hidden" name="amount" value="{{$loan->cycleRepaymentAmount}}" />
                <button type="submit" class="btn btn-info">
                    {{ __('Repay') }}
                </button>
            </form>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">

       <div class="col-md-6">
            <h3>Histories</h3>
            <ul>
                @foreach($loan->status_histories as $sh)
                <li>
                    [{{$sh->created_at}}] - {{$sh->status}}<br/>
                    {{$sh->user_email}}: {{$sh->comment}} 
                </li>
                @endforeach
            </ul>
       </div>
       <div class="col-md-6">
            <h3>Repayment transactions</h3>
            <ul>
                @foreach($loan->repayment_transactions as $trs)
                <li>
                    [{{$trs->created_at}}] {{$trs->user_email}} <br/>
                    Amount: {{$trs->repayment_amount}}</br>
                    Remain: {{$trs->remained_loan_amount}}
                </li>
                @endforeach
            </ul>
       </div>
    </div>
</div>
@endsection
