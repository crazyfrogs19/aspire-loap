@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Code</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Status</th>
                  <th scope="col">Email</th>
                  <th scope="col">Created At</th>
                </tr>
              </thead>
              <tbody>

                @foreach ($data as $loan)
                <tr>
                   <td>
                    <a href="{{route('admin_loan_detail', ['code' => $loan->code])}}">{{$loan->code}}</a>
                  </td>
                  <td>{{$loan->amount}}</td>
                  <td>{{$loan->status}}</td>
                  <td>{{$loan->user->email}}</td>
                  <td>{{$loan->created_at}}</td>
                </tr>
                @endforeach
                
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
