<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Loan\Loan;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Loan::class, function (Faker $faker) {
    $amount = $faker->numberBetween(100,9999);
    $term = $faker->numberBetween(1,48);
    return [
       'code' => Str::uuid(),
       'user_id' => $faker->numberBetween(1,10),
       'amount' => $amount,
       'term_by_week' => $term,
       'weekly_minimum_repay_amount' => ceil($amount/$term),
       'status' => Loan::STATUS_NEW
    ];
});
