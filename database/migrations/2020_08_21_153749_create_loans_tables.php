<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->uuid('code');
            $table->unsignedInteger('user_id');
            $table->smallInteger('term_by_week');
            $table->double('amount', 10, 2);
            $table->double('weekly_minimum_repay_amount', 10, 2);
            $table->date('next_repayment_date', 0)->nullable();
            $table->string('status');
            $table->timestamps();
        });

        Schema::create('loan_status_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('loan_id');
            $table->string('comment')->nullable();
            $table->string('status');
            $table->string('user_email');
            $table->timestamps();
        });

        Schema::create('loan_repayment_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('loan_id');
            $table->string('user_email');
            $table->double('repayment_amount');
            $table->double('remained_loan_amount');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
        Schema::dropIfExists('loan_status_histories');
        Schema::dropIfExists('loan_repayment_transactions');
    }
}
