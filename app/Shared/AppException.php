<?php
namespace App\Shared;

class AppException {

    static function LoanNotFound() {
        $exception = AppValidationException::withMessages([
            'loan' => 'Loan not found',
        ]);
        $exception->status = 404;
        return $exception;
    }

    static function LoanInvalidStatus() {
        $exception = AppValidationException::withMessages([
            'loan' => 'Loan not found',
        ]);
        $exception->status = 400;
        return $exception;
    }

}
