<?php

namespace App\Loan;

use Illuminate\Database\Eloquent\Model;

class LoanStatusHistory extends Model
{
    protected $fillable = [
        'loan_id',
        'status',
        'comment',
        'user_email',
    ];
}
