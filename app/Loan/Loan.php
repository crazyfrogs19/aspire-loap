<?php

namespace App\Loan;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    const STATUS_NEW = 'new';
    const STATUS_REJECTED = 'rejected';
    const STATUS_APPROVED = 'approved';
    const STATUS_COMPLETED = 'completed';

    protected $fillable = [
        'code',
        'user_id',
        'amount',
        'term_by_week',
        'weekly_minimum_repay_amount',
        'status'
    ];

    public function status_histories() {
        return $this->hasMany('App\Loan\LoanStatusHistory');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function repayment_transactions() {
        return $this->hasMany('App\Loan\LoanRepaymentTransaction');
    }

    public function getIsApprovableAttribute() {
        return $this->status == self::STATUS_NEW;
    }

    public function getIsRepayableAttribute() {
        return $this->status == self::STATUS_APPROVED;
    }

    public function getPaidAmount() {
        return $this->repayment_transactions
                    ->map( function($trs) { return floatval($trs->repayment_amount); })
                    ->sum();
    }

    public function getCycleRepaymentAmountAttribute() {
        $paidAmount = $this->getPaidAmount();
        $loanAmount = floatval($this->amount);
        $weeklyMinimumRepayAmount = floatval($this->weekly_minimum_repay_amount);

        if ($loanAmount - $paidAmount > $weeklyMinimumRepayAmount) {
            return $weeklyMinimumRepayAmount;
        }
        return $loanAmount - $paidAmount;
    }
}
