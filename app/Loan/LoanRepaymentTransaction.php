<?php

namespace App\Loan;

use Illuminate\Database\Eloquent\Model;

class LoanRepaymentTransaction extends Model
{
    //
    protected $fillable = [
        'loan_id',
        'user_email',
        'repayment_amount',
        'remained_loan_amount'
    ];

}
