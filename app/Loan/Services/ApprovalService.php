<?php
namespace App\Loan\Services;

use App\Loan\{Loan, LoanStatusHistory};
use App\Shared\AppException;
use Illuminate\Support\{Arr};
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ApprovalService extends AbstractService {

    protected function validate(array $params = [])
    {
       $validator = Validator::make($params, [
            'code' => 'required|uuid',
            'user_email' => 'required|string',
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }

    protected function process(array $params = []) {
        $loan = Loan::where('code', Arr::get($params, 'code'))->first();

        if (empty($loan->id)) {
            throw AppException::LoanNotFound();
        } elseif ($loan->status != Loan::STATUS_NEW) {
            throw AppException::LoanInvalidStatus();
        }

        DB::beginTransaction();
        try {
            $nextRepaymentDate = new \DateTime('+ 1 week');

            $loan->next_repayment_date = $nextRepaymentDate->format('Y-m-d');
            $loan->status = Loan::STATUS_APPROVED;
            $loan->save();

            $statusHistory = LoanStatusHistory::create([
                'loan_id' => $loan->id,
                'status' => $loan->status,
                'comment' => 'Admin approve loan application',
                'user_email' => Arr::get($params, 'user_email'),
            ]);

            DB::commit();

            $this->result = $loan;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
