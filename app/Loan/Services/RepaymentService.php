<?php

namespace App\Loan\Services;

use App\Loan\{
    Loan,
    LoanStatusHistory,
    LoanRepaymentTransaction
};
use App\Shared\AppException;
use Illuminate\Support\{Arr, Str};
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RepaymentService extends AbstractService {

    protected function validate(array $params = []) {
        $validator = Validator::make($params, [
            'user_id' => 'required',
            'user_email' => 'required',
            'code' => 'required|uuid',
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }


    protected function process(array $params = []) {
        $loan = Loan::with(['repayment_transactions'])
            ->where('code', Arr::get($params, 'code'))
            ->where('user_id', Arr::get($params, 'user_id'))
            ->first();

        if (empty($loan->id)) {
            throw AppException::LoanNotFound();
        } elseif ($loan->status != Loan::STATUS_APPROVED) {
            throw AppException::LoanInvalidStatus();
        }

        DB::beginTransaction();
        try {
            $repaymentAmount = floatval(Arr::get($params, 'amount'));
            $paidAmount = $loan->getPaidAmount();
            $remainedLoanAmount = $loan->amount - ($paidAmount + $repaymentAmount);

            $transaction = LoanRepaymentTransaction::create([
                'loan_id' => $loan->id,
                'user_email' => Arr::get($params, 'user_email'),
                'repayment_amount' => $repaymentAmount,
                'remained_loan_amount' => $remainedLoanAmount
            ]);

            if ($remainedLoanAmount <= 0) {
                $loan->status = Loan::STATUS_COMPLETED;
            } else {
                $nextRepaymentDate = (new \DateTime($loan->next_repayment_date))
                    ->add(\DateInterval::createFromDateString('+1 week'));
                $loan->next_repayment_date = $nextRepaymentDate->format('Y-m-d');
            }
            $loan->save();

            $statusHistory = LoanStatusHistory::create([
                'loan_id' => $loan->id,
                'status' => $loan->status,
                'comment' => sprintf('User make repayment - amount %d', $repaymentAmount),
                'user_email' => Arr::get($params, 'user_email'),
            ]);

            DB::commit();

            $this->result = $loan;
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
