<?php
namespace App\Loan\Services;

use App\Loan\Loan;
use Illuminate\Support\{Arr};
use Illuminate\Support\Facades\Validator;

class ListingService extends AbstractService {

    protected function validate(array $params = [])
    {
       $validator = Validator::make($params, [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }

    protected function process(array $params = []) {
        $queryBuilder = Loan::where('user_id', intval(Arr::get($params, 'user_id')));

        $this->result = $queryBuilder->get();
    }

}
