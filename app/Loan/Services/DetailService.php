<?php
namespace App\Loan\Services;

use App\Loan\Loan;
use App\Shared\AppException;
use Illuminate\Support\{Arr};
use Illuminate\Support\Facades\Validator;

class DetailService extends AbstractService {

    protected function validate(array $params = [])
    {
       $validator = Validator::make($params, [
            'code' => 'required|uuid',
            'user_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }

    protected function process(array $params = []) {
        $loan = Loan::with(['user', 'status_histories', 'repayment_transactions'])
            ->where('code', Arr::get($params, 'code'))
            ->where('user_id', Arr::get($params, 'user_id'))
            ->first();

        if (empty($loan->id)) {
            throw AppException::LoanNotFound();
        }

        $this->result = $loan;
    }

}
