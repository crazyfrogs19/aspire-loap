<?php
namespace App\Loan\Services;

use App\Loan\Loan;
use App\Shared\AppException;
use Illuminate\Support\{Arr};
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AdminDetailService extends AbstractService {

    protected function validate(array $params = [])
    {
       $validator = Validator::make($params, [
            'code' => 'required|uuid',
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }

    protected function process(array $params = []) {
        $loan = Loan::with(['user', 'status_histories', 'repayment_transactions'])
            ->where('code', Arr::get($params, 'code'))
            ->first();

        if (empty($loan->id)) {
            throw AppException::LoanNotFound();
        }

        $this->result = $loan;
    }

}
