<?php
namespace App\Loan\Services;

use App\Loan\Loan;
use Illuminate\Support\{Arr};
use Illuminate\Support\Facades\Validator;

class AdminListingService extends AbstractService {

    protected function validate(array $params = [])
    {
        return;
    }

    protected function process(array $params = []) {
        $this->result = Loan::with(['user'])->get();
    }

}
