<?php
namespace App\Loan\Services;

use App\Shared\AppException;
use Illuminate\Validation\Validator;
use App\Shared\AppValidationException;

abstract class AbstractService {

    public $result;
    
    public function execute(array $params) {
        $this->validate($params);
        $this->process($params);
    }

    abstract protected function validate(array $params = []);

    protected function handleFailValidation(Validator $validator) {
        $ex = new AppValidationException($validator);
        $ex->status = 400;
        throw $ex;
    }

    protected function getError() : string {
        return $this->errors;
    }

    abstract protected function process(array $params = []);

    public function getResult() {
        return $this->result;
    }
}
