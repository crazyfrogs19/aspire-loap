<?php

namespace App\Loan\Services;

use App\Loan\{
    Loan,
    LoanStatusHistory
};
use Illuminate\Support\{Arr, Str};
use Illuminate\Support\Facades\Validator;

class RegisterService extends AbstractService {

    protected function validate(array $params = [])
    {
        $validator = Validator::make($params, [
            'user_id' => 'required',
            'user_email' => 'required',
            'amount' => 'required|numeric|min:1',
            'term_by_week' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            $this->handleFailValidation($validator);
        }
    }

    protected function process(array $params = []) {

        $loanAmount = floatval(Arr::get($params,'amount'));
        $loanTermByWeek = intval(Arr::get($params, 'term_by_week'));

        $weeklyMinimumRepayAmount = ceil($loanAmount / $loanTermByWeek);

        $loan = Loan::create([
            'code' => Str::uuid(),
            'user_id' => intval(Arr::get($params, 'user_id')),
            'amount' => $loanAmount,
            'term_by_week' => $loanTermByWeek,
            'weekly_minimum_repay_amount' => $weeklyMinimumRepayAmount,
            'status' => Loan::STATUS_NEW
        ]);

        $statusHistory = LoanStatusHistory::create([
            'loan_id' => $loan->id,
            'status' => $loan->status,
            'comment' => 'User submit loan registration',
            'user_email' => Arr::get($params, 'user_email'),
        ]);

        $this->result = $loan;
    }
}
