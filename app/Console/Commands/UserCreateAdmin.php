<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserCreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@aspire.test',
            'password' => Hash::make('admin@aspire'),
        ]);
        $admin->setRole(User::ROLE_ADMIN);

        printf("\nDefault admin created!\nAccount: admin@aspire.test\nPassword: admin@aspire");
    }
}
