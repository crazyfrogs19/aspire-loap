<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loan\Services\{
    DetailService,
    ListingService,
    RepaymentService,
    RegisterService
};

class LoanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function dashboard()
    {
        $listingService = new ListingService();
        $listingService->execute([
            'user_id' => auth()->user()->id
        ]);
        $allLoans = $listingService->getResult();

        return view('loan.dashboard', [
            'data' => $allLoans
        ]);
    }

    public function detail(Request $request, $code) {
        $detailService = new DetailService();
        $detailService->execute([
            'code' => $code,
            'user_id' => auth()->user()->id 
        ]);
        try {
            $loan = $detailService->getResult();
            return view('loan.detail', ['loan' => $loan]);
        } catch (\App\Shared\AppValidationException $e) {
            abort($e->status);
        }
    }

    public function repay(Request $request, $code) {
        $validatedData = $request->validate([
            'amount' => 'required|numeric|min:1',
        ]);

        $repaymentService = new RepaymentService();
        $repaymentService->execute([
            'user_id' => auth()->user()->id,
            'user_email' => auth()->user()->email,
            'code' => $code,
            'amount' => $request->input('amount'),
        ]);
        
        return redirect()->route('loan_detail', ['code' => $code]);
    }

    public function register(Request $request) {
        $validatedData = $request->validate([
            'amount' => 'required|numeric|min:1',
            'term' => 'required|numeric|min:1',
        ]);
        
        $registerService = new RegisterService;
        $registerService->execute([
            'user_id' => auth()->user()->id,
            'user_email' => auth()->user()->email,
            'amount' => $request->input('amount'),
            'term_by_week' => $request->input('term') 
        ]);

        return redirect()->route('loan_dashboard');
    }

    public function registerForm() {
        return view('loan.register');
    }

}
