<?php

namespace App\Http\Controllers;

use App\Loan\Services\{
    AdminDetailService,
    AdminListingService,
    ApprovalService,
    RejectService
};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AdminLoanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        Gate::authorize('list-all-loans');
        $listingService = new AdminListingService();
        $listingService->execute([
            'user_id' => auth()->user()->id
        ]);
        $allLoans = $listingService->getResult();

        return view('admin.loan.dashboard', [
            'data' => $allLoans
        ]);
    }

    public function approve(Request $request, $code) {
        Gate::authorize('approve-loans');
        $approvalService = new ApprovalService();
        $approvalService->execute([
            'code' => $code,
            'user_email' => auth()->user()->email
        ]);
        return redirect()->route('admin_loan_dashboard');
    }

    public function reject(Request $request, $code) {
        Gate::authorize('reject-loans');
        $approvalService = new RejectService();
        $approvalService->execute([
            'code' => $code,
            'user_email' => auth()->user()->email
        ]);
        return redirect()->route('admin_loan_dashboard');
    }

    public function detail(Request $request, $code) {
        $detailService = new AdminDetailService();
        $detailService->execute([
            'code' => $code,
        ]);

        $loan = $detailService->getResult();
        return view('admin.loan.detail', ['loan' => $loan]);
    }


}
