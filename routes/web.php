<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/loan', 'LoanController@dashboard')->name('loan_dashboard');
Route::get('/loan/register', 'LoanController@registerForm')->name('loan_register_form');
Route::post('/loan/register', 'LoanController@register')->name('loan_register');
Route::get('/loan/{code}', 'LoanController@detail')->name('loan_detail');
Route::post('/loan/{code}/repay', 'LoanController@repay')->name('loan_repay');


Route::get('/admin/loan', 'AdminLoanController@dashboard')->name('admin_loan_dashboard');
Route::get('/admin/loan/{code}', 'AdminLoanController@detail')->name('admin_loan_detail');
Route::put('/loan/{code}/approve', 'AdminLoanController@approve')->name('admin_loan_approve');
Route::put('/loan/{code}/reject', 'AdminLoanController@reject')->name('admin_loan_reject');
