# Requirements
```
Build a simple website that allows an authenticated user to go through a loan application (doesn’t have to contain too many fields, but at least “amount required” and “loan term”). 
All the loans will be assumed to have a “weekly” repayment frequency.  
After the loan is approved the user must be able to submit the weekly loan repayments. 
It can be a simple repay button, which won’t need to check if the dates are correct, but will just set the weekly amount to be repaid.  
—
Notes:
* The chosen libs and tools may proof your experience using them.
* Remember to add meaningful tests to the project.
* You don’t need to waste time doing blade views. We just need good API.
* Source code is more important than design however it is still an important point.
* When your project is done, please share with us as public GitHub/Bitbucket/GitLab repo
* README.md should contains all the information that the reviewers need to run and use the app.
* Breakdown the requirement as User Story Format to explain what did you do.

```
# Installation
Init database & create default admin account

Admin login:
  * email: admin@aspire.test
  * password: admin@aspire

```
$ chmod +x ./setup.sh
$ ./setup.sh
```
Start dev server with local domain http://localhost:8000
```
$ php artisan serve
```
Runt test
```
$ php artisan test
```
# Site maps
1. Basic UI (Laravel default)

| url | description |
|-|-|
| http://localhost:8000/home | Home |
| http://localhost:8000/register | Account registration |
| http://localhost:8000/login | Login |
| http://localhost:8000/logout | Logout |
2. Client UI:

| url | description |
|-|-|
| http://localhost:8000/loan | Loan dashboard |
| http://localhost:8000/loan/regiser | Loan registration application |
| http://localhost:8000/loan/{loan-code} | Loan detail |

4. Admin UI

| url | description |
|-|-|
| http://localhost:8000/admin/loan | Admin loan management dashboard |
| http://localhost:8000/admin/loan/{loan-code} | Admin loan management view loan detail |

# Story
## Client view loan
1. User login (register new if needs)
2. Loan dash board show all loans belongs to current login user. Each row in loan table provide infomation about loan including `code`, `amount`, `status`, `created_at`
3. Client only accessable to their own loans
## Client  register loan application
1. Logged in user go loan register page, provide loan amount and loan term info then submit form
2. New created loan appears in loan dashboard
3. On loan detail, there is no button yet
## Admin approve loan application
1. Admin login, loan application of all users are listed on admin loan dashboard.
2. View loan detail, "Approve" button only shows when loan application status equal to `new`. Admin click on "Approve" button. Loan application change status to `approved`
3. All changes are logged and displayed on loan detail
## Client make repayment
1. Client go to loan detail, "Repay" button appears only when loan status equal `approved`
2. Each time client click button "Repay", new payment transaction was born. Transaction amount was set default by value of the calculation `loan_amount / loan_term`
3. Transactions info is displayed on loan detail
4. Loan status auto update to `complete` when sum all payment transaction amount equal or greater than loan amount, "Repayment" button disappear then
## Admin rejcet loan application
1. Admin open loan detail, "Reject" button only shows when loan application status equal to `new`. Admin click on "Reject" button. Loan application change status to `approved`

# Models
## User
### Properties
+ id
+ username
+ password
+ role
### Actions
1. Login
2. Logout
3. Register

## Loan
### Properties
+ id
+ user_id
+ amount
+ term_by_week
+ status
+ weekly_minimum_repay_amount
### Actions
1. Create
2. Listing
3. Detail
4. Approve/Reject
5. Repayment

## Loan history
### Properties
+ id
+ loan_id
+ status
+ comment
+ username
+ created_at

## Loan repayment transactions
### Properties
+ id
+ loan_id
+ repayment_amount
+ remained_amount
+ username
+ created_at

# Project structure

```
▾ app/
  ▾ Console/
    ▾ Commands/
        UserCreateAdmin.php
      Kernel.php
  ▸ Exceptions/
  ▾ Http/
    ▾ Controllers/
      ▸ Auth/
        AdminLoanController.php
        Controller.php
        HomeController.php
        LoanController.php
    ▸ Middleware/
      Kernel.php
  ▾ Loan/
    ▾ Services/
        AbstractService.php
        AdminDetailService.php
        AdminListingService.php
        ApprovalService.php
        DetailService.php
        ListingService.php
        RegisterService.php
        RejectService.php
        RepaymentService.php
      Loan.php
      LoanRepaymentTransaction.php
      LoanStatusHistory.php
  ▸ Providers/
  ▸ Shared/
    User.php
▸ bootstrap/
▸ config/
▸ database/
▸ public/
▾ resources/
  ▸ js/
  ▸ lang/
  ▸ sass/
  ▾ views/
    ▾ admin/loan/
        dashboard.blade.php
        detail.blade.php
    ▸ auth/
    ▸ layouts/
    ▾ loan/
        dashboard.blade.php
        detail.blade.php
        register.blade.php
      home.blade.php
      welcome.blade.php
▸ routes/
▸ storage/
▾ tests/
  ▾ Feature/
      LoanServicesTest.php
      UserTest.php
  ▸ Unit/
    CreatesApplication.php
    TestCase.php
▸ vendor/
  artisan*
  composer.json
  composer.lock
  package.json
  phpunit.xml
  README.md
  reset.sh*
  server.php
  setup.sh*
  webpack.mix.js
```
