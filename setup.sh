#!/bin/bash

if test -f 'composer.phar' 
then
    php composer.phar install
elif ! command -v composer &> /dev/null 
then
    echo "Please install php composer https://getcomposer.org/download/"
    exit
else
    composer install
fi

touch database/database.sqlite
touch database/database-test.sqlite

php artisan migrate
php artisan migrate --env=testing

php artisan db:seed
php artisan user:create-admin
