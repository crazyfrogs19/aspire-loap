<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanServicesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    
    protected function approveLoan($loan) {
        $approveService = new \App\Loan\Services\ApprovalService();
        $approveService->execute([
            'code' => strval($loan->code),
            'user_email' => 'admin@aspire.test'
        ]);
        return $approveService->getResult();
    }

    public function testListingService() {
        $client1 = factory(\App\User::class)->create();
        $client2 = factory(\App\User::class)->create();
        
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client1->id,
            'user_email' => $client2->email,
            'amount' => 999,
            'term_by_week' => 3
        ]);

        $listingService = new \App\Loan\Services\ListingService();
        $listingService->execute(['user_id' => $client1->id]);
        $loanCollections = $listingService->getResult();

        $loan = $loanCollections->first();

        $this->assertTrue($loan->user_id == $client1->id);

        $listingService->execute(['user_id' => $client2->id]);
        $loanCollections = $listingService->getResult();
        $this->assertTrue($loanCollections->count() == 0);

    }

    public function testRegisterService()
    {
        $client = factory(\App\User::class)->create();
        
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 999,
            'term_by_week' => 3
        ]);

        $result = $resgisterService->getResult();
        $isLoanObject = $result instanceof \App\Loan\Loan;
        $this->assertTrue($isLoanObject);
        $this->assertEmpty($result->next_repayment_date);
    }

    public function testRegisterServiceWithInvalidAmountException()
    {
        $this->expectException(\Illuminate\Validation\ValidationException::class);
        $client = factory(\App\User::class)->create();
        
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 0,
            'term_by_week' => 3
        ]);
    }

    public function testRegisterServiceWithInvalidLoanTermException()
    {
        $this->expectException(\Illuminate\Validation\ValidationException::class);
        $client = factory(\App\User::class)->create();
        
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 99,
            'term_by_week' => 0
        ]);
    }

    public function testApprovalService() 
    {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();

        $approveService = new \App\Loan\Services\ApprovalService();
        $approveService->execute([
            'code' => strval($loan->code),
            'user_email' => 'admin@aspire.test'
        ]);

        $loan = $approveService->getResult();

        $this->assertTrue($loan->status == 'approved');
        $this->assertNotNull($loan->next_repayment_date);
    }

    public function testApprovalServiceWithInvalidStatusException() {
        $this->expectException(\Illuminate\Validation\ValidationException::class);
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();

        $loan->status = 'rejected';
        $loan->save();

        $this->approveLoan($loan);

        $loan = $approveService->getResult();

        $this->assertFalse($loan->status == 'approved');
    }

    public function testRejectService() {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();

        $approveService = new \App\Loan\Services\RejectService();
        $approveService->execute([
            'code' => strval($loan->code),
            'user_email' => 'admin@aspire.test'
        ]);

        $loan = $approveService->getResult();

        $this->assertTrue($loan->status == 'rejected');
    }

    public function testCountStatusHistoryLogOnAction() {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();

        $approveService = new \App\Loan\Services\RejectService();
        $approveService->execute([
            'code' => strval($loan->code),
            'user_email' => 'admin@aspire.test'
        ]);

        $this->assertTrue($loan->status_histories->count() == 2);
    }

    public function testRepaymentServiceWithInvalidStatusException() {
        $this->expectException(\Illuminate\Validation\ValidationException::class);
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();

        $repaymentService = new \App\Loan\Services\RepaymentService();
        $repaymentService->execute([
            'code' => strval($loan->code),
            'amount' => 3
        ]);
        $loan = $repaymentService->getResult();
    }

    public function testCountRepaymentTransactions() {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();
        $loanCode = strval($loan->code);

        $this->approveLoan($loan);
        $repaymentService = new \App\Loan\Services\RepaymentService();
        $repaymentService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'code' => $loanCode,
            'amount' => 3
        ]);
        $this->assertTrue($loan->repayment_transactions->count()==1);
        $this->assertTrue($loan->getPaidAmount() > 0);
    }

    public function testRepaymentServiceAutoChangeStatusToCompletedOnLastSuscessRepaymentTransaction() {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();
        $loanCode = strval($loan->code);

        $this->approveLoan($loan);

        for ($i = 0; $i < 3; $i++) {
            $repaymentService = new \App\Loan\Services\RepaymentService();
            $repaymentService->execute([
                'user_id' => $client->id,
                'user_email' => $client->email,
                'code' => $loanCode,
                'amount' => 3
            ]);
            $loan = $repaymentService->getResult();

            if ($i < 2) {
                $this->assertTrue($loan->status == 'approved');
            } else {
                $this->assertTrue($loan->status == 'completed');
            }
        }
    }

    public function testRepaymentServiceNextRepaymentDateUpdateOnAction() {
        $client = factory(\App\User::class)->create();
        $resgisterService = new \App\Loan\Services\RegisterService();
        $resgisterService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'amount' => 9,
            'term_by_week' => 3
        ]);

        $loan = $resgisterService->getResult();
        $loanCode = strval($loan->code);

        $loan = $this->approveLoan($loan);

        $previouseRepaymentDate = $loan->next_repayment_date;
        $repaymentService = new \App\Loan\Services\RepaymentService();
        $repaymentService->execute([
            'user_id' => $client->id,
            'user_email' => $client->email,
            'code' => $loanCode,
            'amount' => 3
        ]);
        $this->assertTrue($previouseRepaymentDate != $repaymentService);

    }
}
