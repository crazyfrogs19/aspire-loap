<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\{Str};
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testAdminRole() {
        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->assertTrue($admin->isAdmin());
    }

    public function testClientRole() {
        $client = factory(\App\User::class)->create();

        $this->assertFalse($client->isAdmin());
    }

    public function testAuthorizeAdminPage() {
        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $response = $this->actingAs($admin)
                         ->get('/admin/loan');
        $response->assertStatus(200);

        $client = factory(\App\User::class)->create();
        $response = $this->actingAs($client)
                         ->get('/admin/loan');
        $response->assertStatus(403);
    }

    public function testViewNoneExistLoan() {
        $client = factory(\App\User::class)->create();
        $response = $this->actingAs($client)
                         ->get('/loan/'.strval(Str::uuid()));

        $response->assertStatus(404);
    }

    public function testAccessOtherUserLoan() {
        $client1 = factory(\App\User::class)->create();

        $loan = factory(\App\Loan\Loan::class)->create([
            'code' => Str::uuid(),
            'user_id' => $client1->id,
            'amount' => 9,
            'term_by_week' => 3,
            'weekly_minimum_repay_amount' => 3,
            'status' => 'new'
        ]);

        $client2 = factory(\App\User::class)->create();

        $response = $this->actingAs($client2)
                         ->get('/loan/'.strval($loan->code));

        $response->assertStatus(404);
    }

    public function testAdminViewClientLoanDetail() {

        $client1 = factory(\App\User::class)->create();

        $loan = factory(\App\Loan\Loan::class)->create([
            'code' => Str::uuid(),
            'user_id' => $client1->id,
            'amount' => 9,
            'term_by_week' => 3,
            'weekly_minimum_repay_amount' => 3,
            'status' => 'new'
        ]);

        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);
        $response = $this->actingAs($admin)
                         ->get('/admin/loan/'.strval($loan->code));

        $response->assertStatus(200);
    }
}

